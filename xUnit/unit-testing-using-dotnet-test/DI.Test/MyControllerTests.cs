using DI.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace DI.Test
{
    public class MyControllerTests
    {
        [Fact]
        public async Task Get_Returns_Ok()
        {
            // Arrange
            var services = new ServiceCollection();
            services.AddScoped<IService, MyService>();
            var serviceProvider = services.BuildServiceProvider();

            var controller = ActivatorUtilities.CreateInstance<MyController>(serviceProvider);

            // Act
            var result = await controller.Get();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Test data", okResult.Value);
        }
    }
}