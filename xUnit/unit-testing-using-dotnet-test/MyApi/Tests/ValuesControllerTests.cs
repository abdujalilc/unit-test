using Controllers;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;

namespace Tests
{
    public class ValuesControllerTests
    {
        private readonly ValuesController _controller;
        public ValuesControllerTests()
        {
            _controller = new ValuesController();
        }
        [Fact]
        public void Get_ReturnsHelloWorld()
        {
            // Arrange
            var fakeValuesController = A.Fake<ValuesController>();
            var expectedResult = "Hello World";

            // Act
            var result = _controller.Get() as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedResult, result.Value);
        }
    }
}