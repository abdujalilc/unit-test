﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            UnitTest1 unitTest1 = new UnitTest1();
            unitTest1.Test_AddMethod();
            unitTest1.Test_MultiplyMethod();
            unitTest1.Test_DivideMethod();
            unitTest1.Test_SubstractMethod();

        }
    }
}
